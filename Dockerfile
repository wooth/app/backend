FROM golang as BUILD
WORKDIR /app
COPY ./app .
RUN go build


FROM redhat/ubi9-micro
WORKDIR /app
COPY --from=BUILD /app/backend .
EXPOSE 80
CMD ["./backend"]
