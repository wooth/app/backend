:hardbreaks-option:

= backend of wooth app


== build and run locally (also for development purposes):


=== without Docker:
```
cd app
go mod init backend
go get github.com/labstack/echo/v4
```

with or without build binary:
```
go run main.go
```

```
go build
./backend
```


=== with Docker:
```
docker buildx build --tag wooth-backend:latest .
docker run --detach --publish 8484:80 wooth-backend
```
